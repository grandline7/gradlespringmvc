package net.pinsit;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import net.pinsit.config.Config;

public class Initializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext servletContex) throws ServletException {
		registerDispatcherServlet(servletContex);
	}

	private void registerDispatcherServlet(ServletContext servletContex) {
		WebApplicationContext dispatcherConext  = createContext(Config.class);
		DispatcherServlet dispatcherServlet = new DispatcherServlet(dispatcherConext);
		ServletRegistration.Dynamic dispatcher = servletContex.addServlet("dispatcher", dispatcherServlet);
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("*.htm");
		
	}

	private WebApplicationContext createContext(Class<?>... annotateClasses ) {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.register(annotateClasses);
		return context;
	}

}

package net.pinsit.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.pinsit.IndexController;

@Configuration
public class Config {
	
	@Bean
	public IndexController indexController() {
		return new IndexController();
	}
}
